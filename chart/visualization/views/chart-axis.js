Object.defineProperty(exports, "__esModule", { value: true });
var axisCommonModule = require("./chart-axis-common");
// NOTE: This is a dummy file used to fix Typescript errors while developing.
// The NativeScript build overwrites it with one of the corresponding .ios.ts or .android.ts
// files from the same folder.
////////////////////////////////////////////////////////////////////////
// LinearAxis
////////////////////////////////////////////////////////////////////////
var LinearAxis = (function (_super) {
    __extends(LinearAxis, _super);
    function LinearAxis() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return LinearAxis;
}(axisCommonModule.LinearAxis));
exports.LinearAxis = LinearAxis;
////////////////////////////////////////////////////////////////////////
// CategoricalAxis
////////////////////////////////////////////////////////////////////////
var CategoricalAxis = (function (_super) {
    __extends(CategoricalAxis, _super);
    function CategoricalAxis() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return CategoricalAxis;
}(axisCommonModule.CategoricalAxis));
exports.CategoricalAxis = CategoricalAxis;
////////////////////////////////////////////////////////////////////////
// DateTimeContinuousAxis
////////////////////////////////////////////////////////////////////////
var DateTimeContinuousAxis = (function (_super) {
    __extends(DateTimeContinuousAxis, _super);
    function DateTimeContinuousAxis() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return DateTimeContinuousAxis;
}(axisCommonModule.DateTimeContinuousAxis));
exports.DateTimeContinuousAxis = DateTimeContinuousAxis;
////////////////////////////////////////////////////////////////////////
// DateTimeCategoricalAxis
////////////////////////////////////////////////////////////////////////
var DateTimeCategoricalAxis = (function (_super) {
    __extends(DateTimeCategoricalAxis, _super);
    function DateTimeCategoricalAxis() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return DateTimeCategoricalAxis;
}(axisCommonModule.DateTimeCategoricalAxis));
exports.DateTimeCategoricalAxis = DateTimeCategoricalAxis;
////////////////////////////////////////////////////////////////////////
// LogarithmicAxis
////////////////////////////////////////////////////////////////////////
var LogarithmicAxis = (function (_super) {
    __extends(LogarithmicAxis, _super);
    function LogarithmicAxis() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return LogarithmicAxis;
}(axisCommonModule.LogarithmicAxis));
exports.LogarithmicAxis = LogarithmicAxis;
